## -*- Python -*-

rule get_orfs:
    input:
        'test/environm_355DEZ.fasta'
    output:
        'test/DEZ_orfs.fasta'
    shell:
        """
        getorf -circular Y -find 1 -sequence {input} -outseq {output}
        """
