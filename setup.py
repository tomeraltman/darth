from setuptools import setup

setup(
        name='darth',
        version='0.1',
        description='Annotate coronavirus genomes using VADR and other tools.',
        author='Tomer Altman',
        author_email='python@me.tomeraltman.net',
        packages=['darth'],
        install_requires=['biopython','docopt','schema'], ##,'scikit-learn','scikit-plot','tkinter'],
        classifiers=[
                    "Programming Language :: Python :: 3",
                    "License :: OSI Approved :: Mozilla License",
                    "Operating System :: Linux",
                    ],
        include_package_data=True)
