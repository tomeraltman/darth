#!/bin/bash

match_file="$1"     ## hmmsearch-matches.txt
align_sto_file="$2" ## match-algnments.sto


## Convert the alignments into a better format for merging across samples:
awk 'NR==FNR && !/^#/            { pfam[($1 "/" $18 "-" $19)] = $4; next }
     !/^#/ && !/^$/ && !/^\/\/$/ { id = $1; line[id]=line[id] $2; next }
     END                         { for(id in line) { print ">" pfam[id] " " id; print line[id] } }' \
	 $match_file \
	 $align_sto_file
