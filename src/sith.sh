#!/bin/bash

num_cpu=$1  ## Use 8
root_dir=$2 ## Use /darth-out

## HtraNV:
darth.sh SRR8389791 $root_dir/inputs/SRR8389791.epsy.fa none /root/data $root_dir/outputs/HtraNV $num_cpu

## AmexNV
darth.sh SRR6788790 $root_dir/inputs/SRR6788790.epsy.fa none /root/data $root_dir/outputs/AmexNV $num_cpu

## PtetNV
darth.sh SRR7507741 $root_dir/inputs/SRR7507741.epsy.fa none /root/data $root_dir/outputs/PtetNV $num_cpu

## HkudNV
darth.sh SRR1324965 $root_dir/inputs/SRR1324965.epsy.fa none /root/data $root_dir/outputs/HkudNV $num_cpu

## StypNV
darth.sh ERR3994223 $root_dir/inputs/ERR3994223.epsy.fa none /root/data $root_dir/outputs/StypNV $num_cpu

## TparNV
darth.sh SRR10917299 $root_dir/inputs/SRR10917299.epsy.fa none /root/data $root_dir/outputs/TparNV $num_cpu

## AcaNV
darth.sh SRR5997671 $root_dir/inputs/SRR5997671.fa none /root/data $root_dir/outputs/AcaNV $num_cpu

## SilNV
darth.sh SRR12184956 $root_dir/inputs/SRR12184956.fa none /root/data $root_dir/outputs/SilNV $num_cpu

## MalbNV
darth.sh SRR10402291 $root_dir/inputs/SRR10402291.fa none /root/data $root_dir/outputs/MalbNV $num_cpu
