import random, copy, datetime, os, gzip
from collections import defaultdict
import xml.etree.ElementTree as ET
from Bio import SeqIO
from Bio import Entrez
from utils import fetch_ncbi_sample_accession, fetch_ncbi_seq_platform

defer_text = "See sample listed under 'sample derived from' for metadata."

common_fields = {
    'project name': 'Serratus Project',
    'relevant electronic resources': 'https://serratus.io',
    'relevant standard operating procedures': 'https://www.biorxiv.org/content/10.1101/2020.08.07.241729v2',
    'sample derived from': None,
    'tax_id': None,
    'scientific_name': None,
    'sample_alias': None,
    'sample_title': None,
    'sample_description': 'The Serratus Project identified previously-submitted samples in the NCBI SRA which had viral signatures, assembled the runs, and identified novel deltavirus genomes. This sample record is a link between the viral genome and the original (meta-)transcriptomic sample record. See the record listed under “sample derived from” for the original study metadata.'
}

host_sample_fields = {
    'geographic location': 'not applicable',
    'host common name': defer_text,
    'host subject id': defer_text,
    'host health state': 'not applicable',
    'host sex': 'not applicable',
    'host scientific name': defer_text,
    'collector name': defer_text,
    'collecting institution': defer_text,
    'isolate': defer_text
}

miuvigs_sample_fields = {
    'sequencing method': defer_text,
    'assembly software': None,
    'assembly quality': None,
    'investigation type': 'metatranscriptome',
    'source of UViGs': 'metatranscriptome (not viral targeted)',
    'virus enrichment approach': 'other',
    'predicted genome type': None,
    'predicted genome structure': None,
    'detection type': 'independent sequence (UViG)'
    'viral identification software': None,
    'isolation_source': defer_text,
    'collection date': 'not collected',
    'geographic location (country and/or sea)': 'not applicable',
    'geographic location (latitude)': 'not collected',
    'geographic location (longitude)': 'not collected',
    'environment (biome)': defer_text,
    'environment (feature)': defer_text,
    'environment (material)': defer_text,
    'metagenomic source': None
}
    
