import time, os, gzip
import xml.etree.ElementTree as ET
from Bio import SeqIO
from Bio import Entrez

Entrez.email = 'serratus@tomeraltman.net'

def fetch_ncbi_sample_accession(run_id):
    handle = Entrez.efetch(db='sra', id=run_id, retmode='xml')
    xml_root = ET.fromstring(handle.read())
    return xml_root.find('.//SAMPLE').attrib['accession']

def fetch_ncbi_seq_platform(run_id):
    handle = Entrez.efetch(db='sra', id=run_id, retmode='xml')
    xml_root = ET.fromstring(handle.read())
    return xml_root.find('.//PLATFORM')[0].tag

## For DEZ, use topology='circular'
def post_process_embl_file(embl_file_path, topology='linear', project_id='PRJEB44047'):
    ## Post-BioPython format correction
    with open(embl_file_path) as embl_in_fh:
        with gzip.open(embl_file_path + '.gz','wt') as embl_out_fh:
            for raw_line in embl_in_fh:
                ##print(line)
                line = raw_line.strip()
                if line.startswith('ID'):
                    print('ID   XXX; XXX; ' + topology + '; XXX; XXX; XXX; XXX.',
                          file = embl_out_fh)
                elif line.startswith('AC'):
                    id = line.strip().lstrip('AC   ').rstrip(';')
                    print('\n'.join(['AC   XXX;',
                                     'XX',
                                     'AC * _' + id,
                                     'XX',
                                     'PR   Project:' + project_id + ';',
                                     'XX']),
                          file = embl_out_fh)
                else:
                    print(line, file = embl_out_fh)


def print_sample_xml(run_id_list_file, num_secs=1):
    with open(run_id_list_file,'r') as run_ids_fh:
        for run_id_raw in run_ids_fh:
            run_id = run_id_raw.strip()
            if not os.path.isfile('/home/taltman/data/serratus/sample_xml_cache/' + run_id + '.xml'):
                print('Downloading XML for Run ' + run_id)
                try:
                    handle = Entrez.efetch(db='sra', id=run_id, retmode='xml')
                    with open('/home/taltman/data/serratus/sample_xml_cache/' + run_id + '.xml', 'w') as xml_fh:
                        print(handle.read().decode('UTF-8'),
                              file=xml_fh)
                except:
                    with open('/home/taltman/data/serratus/sample_xml_cache/bad_run_ids.txt','a') as bad_fh:
                        print(run_id, file=bad_fh)
                    print('Bad run: ' + run_id)
                time.sleep(num_secs)
            else:
                print('Skipping run ' + run_id + '; already downloaded.')    


