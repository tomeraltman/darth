#!/bin/bash 
username="$1"
password="$2"
## taltman@khaifa:~/data/serratus/dez_submit$

## This dir is full of directories, created by DEZ-dispenser.py (see export_all_dez_genomes):
cd ~/data/serratus/dez_submit

for dir in *
do
    pushd $dir
    echo $dir
    time java -jar ~/bin/webin-cli-4.2.3.jar -userName="$username" -password="$password" -context=genome -manifest=MANIFEST -validate
    popd
done

## If all looks good above, then submit!
rm -f failed_submit.txt submitted.txt
touch failed_submit.txt
touch submitted.txt

for dir in `ls | grep SERRATUS`
do
    pushd $dir
    echo $dir
    if java -jar ~/bin/webin-cli-4.2.3.jar -userName="$username" -password="$password" -context=genome -manifest=MANIFEST -submit
    then
	echo $dir >> ../submitted.txt
    else
	echo $dir >> ../failed_submit.txt
    fi
    popd
done


## Print mapping file between ENA accession, assembly name, and original name from Marcos:
awk -F'\t' 'NR==FNR { id_map[$1] = $2} NR!=FNR { FS="\""; OFS="\t" } /ERROR/ { split(FILENAME,name_parts,"/"); print $6, name_parts[1], id_map[name_parts[1]] }' ~/repos/darth/test/seq_id_map.txt */genome/*/submit/receipt.xml
