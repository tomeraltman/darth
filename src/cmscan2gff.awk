#!/usr/bin/awk -f

## Call with -v seqid="seq_id" -v prog="vadr"

function print_attributes(ftr_key, parent_id, start, end, strand, annot) {

    attributes = "ID=" annot["ID"]";"
    ## Concatenate the annots:
    if ( "Name" in annot)
	attributes = attributes "Name="   annot["Name"]  ";"
    if ( (ftr_key == "CDS" || ftr_key == "misc_feature" || ftr_key == "") && parent_id )
	attributes = attributes "Parent=" parent_id      ";"
    if ( "Alias" in annot)
	attributes = attributes "Alias="  annot["Alias"] ";"
    if ( "Note" in annot)
	attributes = attributes "Note="   annot["Note"]  ";"
    
    ## Print the GFF3 line:
    print seqid,
	prog,
	(ftr_key!="")?ftr_key:"region",
	start,
	end,
	".",
	strand,
	".",
	attributes

}

BEGIN {
    OFS = "\t"
    print "##gff-version 3"
}

/^#/ { next }

$19 == "!" {
    ++ftr_id

    if ( $2 ~ /5UTR/ )
	type = "five_prime_UTR"
    else if ( $2 ~ /3UTR/ )
	type = "three_prime_UTR"
    else if ( $2 ~ /pk/ )
	type = "pseudoknot"
    else if ( $2 ~ /FSE/ )
	type = "cis_regulatory_frameshift_element"

    note = $27
    for(i=28; i<=NF; i++)
	note = note " " $i
    
    ## Print the GFF3 line:
    print seqid,
	prog,
	type, 
	($12=="+")?$10:$11, # start-base
	($12=="+")?$11:$10, # end-base
	$18, # E-value
	$12, # strand
	".",
	"ID=ftr-" ftr_id ";Name=" $2 ";Alias=" $3 ";Note=" note
    
}

