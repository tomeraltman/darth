import random, copy, datetime, os, gzip
from collections import defaultdict
import xml.etree.ElementTree as ET
from Bio import SeqIO
from Bio import Entrez

#from Bio.Alphabet import generic_dna
from Bio.SeqFeature import SeqFeature, FeatureLocation, Reference, BeforePosition, AfterPosition


### Loading data:

## Load hammer-head ribozyme hits:
# dez_hhrbz_aligns = defaultdict(list)
# for record in SeqIO.parse("test/env_355DEZ_hh.sto","stockholm"):
#     dez_hhrbz_aligns[record.name].append(record)
## I could be wrong, but it seems that the start bases are not loaded correctly from Stockholm format into BioPython's internal representation.
## Stockholm format is 1-indexed, while BioPython is zero-indexed, open right interval. So the 'start' attribute of the annotation should be decremented by one.
## TODO Report to BioPython time-permitting.

#dez_hhrbz_aligns = parse_infernal_output('test/env_355DEZ_hh.out')

## Call as:
## dispense_DEZ('test/hhrbz4_environm_355DEZ_v2.out',
##              'test/env_dvrbz_infernal.out',
##              'test/environm_355DEZ_v2.fasta')

## Given an NCBI SRA run ID, and the kind of virus, return the ENA sample accession ID:
sample_id_dict = {
    'SRR8840728':  {'zeta': 'ERS8997237', 'epsilon': 'ERS8997236'},
    'SRR8924823':  {'zeta': 'ERS8997235'},
    'SRR7286070':  {'zeta': 'ERS8997233', 'epsilon': 'ERS8997234'},
    'SRR6943136':  {'zeta': 'ERS8997231', 'epsilon': 'ERS8997232'},
    'SRR6201737':  {'zeta': 'ERS8997229', 'epsilon': 'ERS8997230'},
    'SRR5864109':  {'zeta': 'ERS8997227', 'epsilon': 'ERS8997228'},
    'SRR7170939':  {'epsilon': 'ERS8997238'},
    'SRR12063536': {'zeta': 'ERS8997226'}
}


def dispense_DEZ(hhrbz4_annot_file,
                 dvrbz_annot_file,
                 dez_genomes_file):
    
    dez_hhrbz_aligns = parse_infernal_output(hhrbz4_annot_file)
    dez_dvrbz_aligns = parse_infernal_output(dvrbz_annot_file)
    
    
    ## Step 1: Load genomes & annotations:
    dez_genomes = load_dez_genomes(dez_hhrbz_aligns,
                                   dez_dvrbz_aligns,
                                   dez_genomes_file)
    
    
    ## Step 2: Reconfigure the genomes while exporting the sequence:
    adjusted_dez_genomes = { key:adjust_genome(value) for (key, value) in dez_genomes.items() }
    #adjusted_dez_genomes = [adjust_genome(dez_genomes[genome]) for genome in dez_genomes]
    
    with open('test/adjusted_genomes.fasta','w') as genomes_fh:
        SeqIO.write(list(adjusted_dez_genomes.values()),
                    genomes_fh, "fasta")
    
    ## Step 3: Run getORF on the adjusted genome sequences
    
    '''
    time getorf -minsize 100 -circular Y -find 3 -sequence test/adjusted_genomes.fasta -outseq test/predicted_orfs.fasta
    '''
    ## Step 4: Import the ORFs as features
    
    ## Load ORFfinder annots:
    dez_orf_call_ftrs = defaultdict(list)
    
    genome_id_orf_counter = defaultdict(int)
    for record in SeqIO.parse('test/predicted_orfs.fasta', 'fasta'):
        desc_tokens = record.description.split(' ')
        genome_id = '_'.join(desc_tokens[0].split('_')[0:-1])
        genome_id_orf_counter[genome_id] += 1
        dez_orf_call_ftrs[genome_id].append(orf_desc2feature(record.description,
                                                             genome_id,
                                                             genome_id_orf_counter[genome_id]))
    
    for genome_id in dez_orf_call_ftrs:
        for ftr in dez_orf_call_ftrs[genome_id]:
            adjusted_dez_genomes[genome_id].features.append(ftr)
    
    ## Add the 'source' feature:
    for genome_id in adjusted_dez_genomes:
        genome_len = len(adjusted_dez_genomes[genome_id])
        if genome_len < 789:
            org = 'Zeta virus'
        else:
            org = 'Epsilon virus'
        adjusted_dez_genomes[genome_id].features.insert(0,
                                                        SeqFeature(FeatureLocation(0,
                                                                                   genome_len),
                                                                   type = 'source',
                                                                   strand = 1,
                                                                   qualifiers = {'mol_type': 'genomic RNA',
                                                                                 'organism': org }))
    ## Update record IDs:
    renamed_adjusted_dez_genomes = {}
    id_map = {}
    genome_count = 1
    for record in adjusted_dez_genomes.values():
        ## Standardize the genome name:
        genome_id = 'SERRATUS-DEZ-' + str(genome_count)
        genome_count += 1
        id_map[genome_id] = record.id
        record.id = genome_id
        renamed_adjusted_dez_genomes[genome_id] = record
        
    ## Print out id_map for future reference:
    with open('test/seq_id_map.txt', 'w') as map_fh:
        for (key,value) in id_map.items():
            print(key + '\t' + value, file=map_fh)
    
    
    return renamed_adjusted_dez_genomes, id_map

    ## Removed db_xref qualifier as per Zahra's feedback.
    
    
    ### Step 5: Export updated annotations
    
    ## GFF Export:
    #with open('test/DEZ-annots.gff','w') as gff_fh:
    #    GFF.write(list(adjusted_dez_genomes.values()), gff_fh)
    
    ## EMBL Export:
    
    # with open('test/DEZ-annots.embl','w') as embl_fh:
    #     for genome_id in adjusted_dez_genomes:
    #         genome = adjusted_dez_genomes[genome_id]
    #         #genome.id = 'XXX'
    #         ## Delete the 'tmp_score' qualifiers:
    #         for ftr in genome.features:
    #             if 'tmp_score' in ftr.qualifiers:
    #                 ftr.qualifiers.pop('tmp_score')
    #         SeqIO.write(genome, embl_fh, 'embl')
    
    
    ### Step 6: Post-EMBL Line insertion
    
    ## For the file to be submitted to EMBL in the desired format,
    ## it is necessary to manually "inject" some lines onto the files generated by BioPython.
    ## When possible, I try to get desired output format by storing the corresponding
    ## info in the BioSequence objects. If I can't figure out how to do it via BioPython,
    ## I manually do so here:
    
    # with open('test/DEZ-annots.embl') as embl_in_fh:
    #     with open('test/DEZ-annots-edited.embl','w') as embl_out_fh:
    #         project_id = 'PRJEB44047'
    #         for raw_line in embl_in_fh:
    #             ##print(line)
    #             line = raw_line.strip()
    #             if line.startswith('ID'):
    #                 print('ID   XXX; XXX; circular; XXX; XXX; XXX; XXX.',
    #                       file = embl_out_fh)
    #             elif line.startswith('AC'):
    #                 id = line.strip().lstrip('AC   ').rstrip(';')
    #                 print('\n'.join(['AC   XXX;',
    #                                  'XX',
    #                                  'AC * _' + id,
    #                                  'XX',
    #                                  'PR   Project:' + project_id + ';',
    #                                  'XX']),
    #                       file = embl_out_fh)
    #             else:
    #                 print(line, file = embl_out_fh)
    

## Load genomes as SeqRecords, and add features:
## 

def load_dez_genomes(dez_hhrbz_aligns,
                     dez_dvrbz_aligns,
                     genome_seqs_file):
    dez_genomes = {}
    for record in SeqIO.parse(genome_seqs_file, "fasta"):
        genome_ftr_counter = 0
        #record.id = 'XXX'
        record.name = ''
        record.description = 'XXX'
        ref = Reference()
        ref.authors = 'XXX'
        ref.title = ''
        today = datetime.date.today().strftime('%d-%b-%Y').upper()
        ref.journal = 'Submitted ' + today + ' to the INSDC.'
        record.annotations['references'] = [ref]
        ##record.annotations['molecule_type'] = 'RNA'
        record.annotations['molecule_type'] = 'genomic RNA'
        ##record.annotations['molecule_type'] = 'XXX'
        record.annotations['topology'] = 'circular'
        record.annotations['accessions'] = ['XXX']
        record.annotations['sequence_version'] = 'XXX'
        record.annotations['data_file_division'] = 'XXX'
        ## record.annotations['organism'] = 'DEZ'

        for annot in dez_hhrbz_aligns[record.id]:
            genome_ftr_counter += 1
            record.features.append(create_rbz_feature(annot,
                                                      genome_ftr_counter,
                                                      'Hammer-head ribozyme'))
        for annot in dez_dvrbz_aligns[record.id]:
            genome_ftr_counter += 1
            record.features.append(create_rbz_feature(annot,
                                                      genome_ftr_counter,
                                                      'Deltavirus ribozyme (RFam RF02682)'))

        dez_genomes[record.id] = record
    
    return dez_genomes


    
## Assuming zero-indexed, half-open interval:
def create_rbz_feature(annot,genome_ftr_counter,note):
    ## Also, there's Python slicing notation, so the end interval is "open"!!
    ## https://www.biostars.org/p/139981/
    start = annot['start']
    end   = annot['end']
    strand = 1 if start < end else -1
    new_feature = SeqFeature(FeatureLocation((start if strand == 1 else end),
                                             (end if strand == 1 else start)),
                             type = 'gene',
                             strand = strand,
                             qualifiers = {
                                 'inference': 'COORDINATES:profile:Infernal:1.1.4',
                                 'tmp_score': annot['E-value'],
                                 'locus_tag': 'SERRATUS_gene_' + str(genome_ftr_counter),
                                 'note': note + '. Infernal E-value: ' + str(annot['E-value']),
                                 'product': 'ribozyme'
                             })
    return new_feature
    
    
## We have to dump out the modified genomes to FASTA,
## then have getORF do its thing,
## then import it back and decorate the ORFs onto the annotation.
## Much easier than doing interval arithmetic on joined regions!
 
def orf_desc2feature(desc, genome_id, ftr_id):

    genome_length = len(dez_genomes[genome_id])
    
    desc_tokens = desc.split(' ')
    if 'REVERSE' in desc:
        end   = int(desc_tokens[1].replace('[','') )
        start = int(desc_tokens[3].replace(']','') )
        strand = -1
    else:
        start = int(desc_tokens[1].replace('[','') )
        end   = int(desc_tokens[3].replace(']','') )
        strand = 1
    
    ## this is required because I found three cases where getORF will return an end coordinate of '0':
    if start == 0:
        start = 1
    if end == 0:
        end = 1
    
    qualifiers = {
        'inference': 'COORDINATES:ab initio prediction:getorf:6.6.0.0',
        'locus_tag': 'SERRATUS_orf_' + str(ftr_id),
        'product': 'SERRATUS_orf_' + str(ftr_id),
        'codon_start': '1'
        }
    
    ## ORFs that cross the origin:
    if 'ORF crosses' in desc:
        
        ## For special case of Zeta "endless" ORFs on the positive strand:
        if strand == 1 and (genome_length*3 - end) < 4:
            
            end_pos = AfterPosition(genome_length)
            new_ftr = SeqFeature(FeatureLocation(start-1,end_pos),
                                 strand = strand,
                                 type = 'CDS',
                                 qualifiers = qualifiers)
            new_ftr.qualifiers['note'] = 'Endless ORF'
            
        ## For the special case of Zeta "endless" ORFs on the negative strand:
        elif strand == -1 and end/genome_length > 2 and start < 4:
            
            start_pos = BeforePosition(0)
            new_ftr = SeqFeature(FeatureLocation(start_pos,end-int(end/genome_length)*genome_length),
                                 strand = strand,
                                 type = 'CDS',
                                 qualifiers = qualifiers)
            new_ftr.qualifiers['note'] = 'Endless ORF'
            
            
        ## these cases cross the origin once or twice, but then stop before crossing a third time:
        else:
            
            ## create joined feature
            ftr_loc_start = FeatureLocation(start-1,genome_length)
            ftr_loc_end   = FeatureLocation(0,end-int(end/genome_length)*genome_length)

            ## If we go across the origin two times, then we need a third location:
            if end/genome_length > 2 or (start > 1 and int(end/genome_length) == 2):

                ftr_loc_middle = FeatureLocation(0,genome_length)
                
                if strand == 1:
                    new_ftr = SeqFeature(ftr_loc_start+ftr_loc_middle+ftr_loc_end,
                                         strand = strand,
                                         type = 'CDS',
                                         qualifiers = qualifiers)
                else:
                    new_ftr = SeqFeature(ftr_loc_end+ftr_loc_middle+ftr_loc_start,
                                         strand = strand,
                                         type = 'CDS',
                                         qualifiers = qualifiers)
            else:
                
                if strand == 1:
                    new_ftr = SeqFeature(ftr_loc_start+ftr_loc_end,
                                         strand = strand,
                                         type = 'CDS',
                                         qualifiers = qualifiers)
                else:
                    new_ftr = SeqFeature(ftr_loc_end+ftr_loc_start,
                                         strand = strand,
                                         type = 'CDS',
                                         qualifiers = qualifiers)
                
    else:
        ## create simple feature
        new_ftr = SeqFeature(FeatureLocation(start-1,end),
                             strand = strand,
                             type = 'CDS',
                             qualifiers = qualifiers)
        
    return new_ftr


def genome_stats(genome):
    print(genome.id, len(genome), genome.id in dez_hhrbz_aligns)
    if genome.id in dez_hhrbz_aligns:
        print(len(dez_hhrbz_aligns[genome.id]))

def adjust_genome(genome):
    new_genome = copy.deepcopy(genome)
    if len(new_genome.features) == 0:
        return new_genome
    if len(new_genome.features) >= 1:
        if len(new_genome.features) == 1:
            ref_ftr = new_genome.features[0]
        else:
            ref_ftr = pick_best_feature(new_genome)

        if ref_ftr.strand == -1:

            new_genome = new_genome.reverse_complement(id   = new_genome.id,
                                                       name = new_genome.name,
                                                       description = new_genome.description)
                                                       
            ## Since we took the reverse complement, we have to find the ref_ftr again:
            if len(new_genome.features) == 1:
                ref_ftr = new_genome.features[0]
            else:
                ref_ftr = pick_best_feature(new_genome)

        ## Fix the genome sequence:
        new_genome.seq = new_genome.seq[ref_ftr.location.start:
                                        len(new_genome)] \
                                        + new_genome.seq[0:ref_ftr.location.start]
        ## Fix the features:
        new_ftrs = []
        #print(genome.name)
        for ftr in new_genome.features:
            new_ftr = adjust_feature(ftr,ref_ftr.location.start,new_genome)
            new_ftrs.append(new_ftr)
        new_genome.features = new_ftrs
        
        ## Make sure we haven't lost the molecule_type, due to recent changes in BioPython:
        new_genome.annotations = genome.annotations
        
        return new_genome

    
def adjust_feature(feature, delta, genome):
    ftr_start = feature.location.start - delta
    ftr_end   = feature.location.end   - delta
    
    if ftr_start >= 0 and ftr_end >= 0:
        new_ftr = SeqFeature(FeatureLocation(ftr_start,
                                             ftr_end),
                             strand = feature.strand)
    elif ftr_start < 0 and ftr_end < 0:
        ftr_start = len(genome) + ftr_start
        ftr_end = len(genome) + ftr_end
        new_ftr = SeqFeature(FeatureLocation(ftr_start,
                                             ftr_end),
                             strand = feature.strand)
    elif ftr_start < 0: ## It's impossible for ftr_start >=0 and ftr_end < 0...
        ## We have to split the feature location across two regions, due to spanning the genome origin:
        ## https://biopython.org/docs/1.75/api/Bio.SeqFeature.html
        
        ftr_loc1 = FeatureLocation(len(genome) + ftr_start,
                                   len(genome))
        ftr_loc2 = FeatureLocation(0,
                                   ftr_end)
        
        if feature.strand == 1:
            new_ftr = SeqFeature(ftr_loc1+ftr_loc2,
                                 strand = feature.strand)
        else:
            new_ftr = SeqFeature(ftr_loc2+ftr_loc1,
                                 strand = feature.strand)

    new_ftr.type       = feature.type
    new_ftr.qualifiers = feature.qualifiers
    return new_ftr

def pick_best_feature(genome):
    dv_ftrs = []
    hh_ftrs = []
    for ftr in genome.features:
        if ftr.type != 'source':
            if 'Delta' in ftr.qualifiers['note']:
                dv_ftrs.append(ftr)
            if 'Hammer' in ftr.qualifiers['note']:
                hh_ftrs.append(ftr)
    if len(dv_ftrs) > 0:
        return sorted(dv_ftrs,
                    key = lambda ftr: ftr.qualifiers['tmp_score'])[0]
    else:
        return sorted(hh_ftrs,
                    key = lambda ftr: ftr.qualifiers['tmp_score'])[0]

# def fetch_ncbi_sample_accession(run_id):
#     Entrez.email = 'serratus@tomeraltman.net'
#     handle = Entrez.efetch(db='sra', id=run_id, retmode='xml')
#     xml_root = ET.fromstring(handle.read())
#     return xml_root.find('.//SAMPLE').attrib['accession']

# def fetch_ncbi_seq_platform(run_id):
#     Entrez.email = 'serratus@tomeraltman.net'
#     handle = Entrez.efetch(db='sra', id=run_id, retmode='xml')
#     xml_root = ET.fromstring(handle.read())
#     return xml_root.find('.//PLATFORM')[0].tag
    
def export_dez_genome (genome_id,
                       root_dir,
                       adjusted_dez_genomes,
                       id_map,
                       project_id='PRJEB44047'):
    ## Set up the directory:
    os.makedirs(root_dir + '/' + genome_id,
                exist_ok=True)
    ## Create the Manifest file:
    with open(root_dir + '/' + genome_id + '/MANIFEST','w') as manifest_fh:
        ## Fetch experiment accession using BioPython
        genome_id_parts = id_map[genome_id].split('_')
        sra_run_id = genome_id_parts[0]
        genome_len = len(adjusted_dez_genomes[genome_id])
        if genome_len < 789:
            org = 'zeta'
        else:
            org = 'epsilon'
        coverage = genome_id_parts[genome_id_parts.index('cov')+1]
        seq_platform = fetch_ncbi_seq_platform(sra_run_id)
        print('\n'.join(['STUDY\t' + project_id,
                         'NAME\t' + genome_id,
                         'SAMPLE\t' + sample_id_dict[sra_run_id][org], #fetch_ncbi_sample_accession(sra_run_id),
                         'RUN_REF\t' + sra_run_id,
                         'MOLECULETYPE\tgenomic RNA',
                         'ASSEMBLY_TYPE\tisolate',
                         'COVERAGE\t' + coverage,
                         'PROGRAM\trnaviralSPAdes',
                         'PLATFORM\t' + seq_platform,
                         'FLATFILE\tgenome.embl.gz',
                         'CHROMOSOME_LIST\tchromosomes.gz']),
              file=manifest_fh)

    ## Write the chromosomes.txt file:
    with gzip.open(root_dir + '/' + genome_id + '/chromosomes.gz', 'wt') as chrom_fh:
        print('\t'.join([genome_id,
                         '1',
                         'Circular-Chromosome']),
              file=chrom_fh)
    
    ## Write the EMBL file:
    with open(root_dir + '/' + genome_id + '/genome-temp.embl','wt') as embl_fh:
        genome = adjusted_dez_genomes[genome_id]
        for ftr in genome.features:
            if 'tmp_score' in ftr.qualifiers:
                ftr.qualifiers.pop('tmp_score')
        SeqIO.write(genome, embl_fh, 'embl')

    ## Post-BioPython format correction
    with open(root_dir + '/' + genome_id + '/genome-temp.embl') as embl_in_fh:
        with gzip.open(root_dir + '/' + genome_id + '/genome.embl.gz','wt') as embl_out_fh:
            for raw_line in embl_in_fh:
                ##print(line)
                line = raw_line.strip()
                if line.startswith('ID'):
                    print('ID   XXX; XXX; circular; XXX; XXX; XXX; XXX.',
                          file = embl_out_fh)
                elif line.startswith('AC'):
                    id = line.strip().lstrip('AC   ').rstrip(';')
                    print('\n'.join(['AC   XXX;',
                                     'XX',
                                     'AC * _' + id,
                                     'XX',
                                     'PR   Project:' + project_id + ';',
                                     'XX']),
                          file = embl_out_fh)
                else:
                    print(line, file = embl_out_fh)

def export_all_dez_genomes(adjusted_dez_genomes,
                           id_map,
                           out_dir='/home/taltman/data/serratus/dez_submit'):
    for genome_id in adjusted_dez_genomes.keys():
        export_dez_genome(genome_id, out_dir, adjusted_dez_genomes, id_map)
                    
### Running webin:

## Single run:

## cd /tmp/SRR12063536_12399_length_442_cov_22.517060_curated381nt/
## java -jar ~/bin/webin-cli-4.2.3.jar -userName=<username> -password=<password> -context=genome -validate -manifest=MANIFEST 

        
    # if len(genome.features[0]) > len(genome.features[1]):
    #     anchor_ftr = genome.features[0]
    # elif len(genome.features[1]) > len(genome.features[0]):
    #     anchor_ftr = genome.features[1]
    # else:
    #     ftr1_uppercase = sum(1 for c in str(genome.features[0].qualifiers['alignment']) if c.isupper())
    #     ftr2_uppercase = sum(1 for c in str(genome.features[1].qualifiers['alignment']) if c.isupper())
    #     if ftr1_uppercase > ftr2_uppercase:
    #         anchor_ftr = genome.features[0]
    #     elif ftr1_uppercase < ftr2_uppercase:
    #         anchor_ftr = genome.features[1]
    #     else:
    #         anchor_ftr = genome.features[random.randrange(0,2)] 
    # return anchor_ftr

    # print(genome.id, len(genome))
    # for annot in dez_hhrbz_aligns[genome.id]:
    #     start = annot.annotations['start']
    #     end   = annot.annotations['end']
    #     strand = '+' if start < end else '-'
    #     length = (end-start+1) if strand == '+' else (start-end+1)
    #     print(start, end, strand, length)

### Utilities:

def parse_infernal_output(infernal_path):
    parsed_output = defaultdict(list)
    with open(infernal_path) as infernal_fh:
        for line in infernal_fh:
            tokens = line.strip().split()
            if len(tokens) > 9 and tokens[1] == '!' and tokens[5].startswith('SRR'):
                line_dict = {}
                line_dict['start']   = int(tokens[6]) - 1 ## Converting into BioPython notation
                line_dict['end']     = int(tokens[7])
                line_dict['strand']  = tokens[8]
                line_dict['score']   = float(tokens[3])
                line_dict['E-value'] = float(tokens[2])
                parsed_output[tokens[5]].append(line_dict)
    return parsed_output
                

    
### Interactive reports:
    
## Printing Zetas:
with open('/tmp/zetas.fasta','w') as zetas_fh:
    SeqIO.write([zeta_fix(dez_genomes[genome]) for genome in dez_genomes if len(dez_genomes[genome]) < 789],
                zetas_fh, "fasta")


    
for genome_id in dez_genomes:
    genome = dez_genomes[genome_id]
    if len(genome) < 789 and (len(genome) != len(zeta_fix(genome))):
        print(genome)
    
## Reports for understanding data landscape

for genome_id in dez_genomes:
    genome = dez_genomes[genome_id]
    if genome_length < 789 and len(genome.features) > 1:
        for ftr in genome.features:
            alignment = ftr.qualifiers['alignment']
            print(ftr.location.end - ftr.location.start,
                  sum(1 for c in str(alignment) if c.isupper()) / (ftr.location.end - ftr.location.start))
        print('---')
## Conclusion: if more than one alignment, pick the one that is longer, if the same length, pick the one that has more upper-case aligned residues, otherwise, coin toss.
                  


DEZ_annots = defaultdict(list)

for genome_id in dez_genomes.keys():
    genome = dez_genomes[genome_id]
    genome_length = len(genome)
    if genome_length < 789:
        if genome.id in dez_hhrbz_aligns:
            DEZ_annots['zeta_'+str(len(dez_hhrbz_aligns[genome.id]))].append(genome_id)
        else:
            DEZ_annots['zeta_0'].append(genome_id)
    elif genome_length > 910:
        if genome.id in dez_hhrbz_aligns:
            DEZ_annots['epsilon_'+str(len(dez_hhrbz_aligns[genome.id]))].append(genome_id)
        else:
            DEZ_annots['epsilon_0'].append(genome_id)
    else:
        if genome.id in dez_hhrbz_aligns:
            DEZ_annots['delta_'+str(len(dez_hhrbz_aligns[genome.id]))].append(genome_id)
        else:
            DEZ_annots['delta_0'].append(genome_id)

for genome_id in dez_genomes:
    num_hh = 0
    num_dv = 0
    for ftr in dez_genomes[genome_id].features:
        if 'Deltavirus' in ftr.qualifiers['note']:
            num_dv += 1
        if 'Hammer' in ftr.qualifiers['note']:
            num_hh += 1
    if num_hh == 0:
        print(genome_id)

            
## Reviewing ORFS:
for genome_id in set(list(dez_orf_calls_p.keys()) + list(dez_orf_calls_n.keys())):
    print(genome_id)
    if len(dez_orf_calls_p[genome_id]) > 1:
        print('   ',dez_orf_calls_p[genome_id])
    if len(dez_orf_calls_n[genome_id]) > 1:
        print('   ',dez_orf_calls_n[genome_id])
    
## Any genomes not a multiple of 3 with endless orf?
def nonthree_genome_endless_orfs():
    target_genomes = []
    for genome_id in adjusted_dez_genomes:
        genome = adjusted_dez_genomes[genome_id]
        if (len(genome) % 3 ) != 0:
            for ftr in genome.features:
                print(ftr.qualifiers)
                if 'note' in ftr.qualifiers and 'Endless' in ftr.qualifiers['note']:
                    target_genomes.append(genome_id)
    return list(set(target_genomes))
