import time, os, sys, datetime, gzip
import xml.etree.ElementTree as ET
from collections import defaultdict
from Bio import SeqIO
from Bio import Entrez
from Bio.SeqFeature import SeqFeature, FeatureLocation, Reference, BeforePosition, AfterPosition
from utils import fetch_ncbi_seq_platform, post_process_embl_file

Entrez.email = 'serratus@tomeraltman.net'

## Mapping of non-structural proteins of Coronavirus genomes:
## Descriptions courtesy of:
## https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7682334/
nsp_desc = {
    'nsp1': 'Cellular mRNA degradation',
    'nsp2': 'Binds to prohibitin proteins',
    'nsp3': 'Large multi-domain transmembrane protein',
    'nsp4': 'Potential transmembrane scaffold protein',
    'nsp5': 'Mpro, cleaves viral polyprotein',
    'nsp6': 'Potential transmembrane scaffold protein',
    'nsp7': 'Potential processivity clamp for RNA polymerase',
    'nsp8': 'Potential processivity clamp for RNA polymerase and primase',
    'nsp9': 'RNA binding protein phosphatase',
    'nsp10': "Cofactor for ExoN and 2'-O-MT activity",
    'nsp12': 'Replication enzyme',
    'nsp13': "RNA helicase, 5' triphosphatase",
    'nsp14': "N7 MTase and 3'-5' exoribonuclease, ExoN",
    'nsp15': 'Viral endoribonuclease, NendoU',
    'nsp16': "2'-O-MT"
}

## AcaNV:
## the -validate command for Webin-CLI stated something about multiple 'locus_tag' qualifiers for a CDS.
## I noticed that there was a "N2" gene predicted that was subsumed by the earlier "N" gene.
## Deleting the N2 gene & CDS entry in the genome.embl.gz file allowed the submission to go through.

sample_id_dict = {
    'SRR6788790' : ['AmexNV', 'ERS9105429', 'Ambystoma mexicanum nidovirus'     ], ## hand-fixed N2 issue, submitted
    'SRR7507741' : ['PtetNV', 'ERS9105430', 'Puntigrus tetrazona nidovirus'     ], ## submitted
    'SRR1324965' : ['HkudNV', 'ERS9105431', 'Hippocampus kuda nidovirus'        ], ## rerun VADR
    'ERR3994223' : ['StypNV', 'ERS9105432', 'Syngnathus typhle nidovirus'       ], ## submitted
    'SRR10917299': ['TparNV', 'ERS9105433', 'Takifugu pardalis nidovirus'       ], ## fixed parsing, submitted
    'SRR5997671' : ['AcaNV',  'ERS9105434', 'Acanthemblemaria sp. nidovirus'    ], ## hand-fixed validation error, submitted
    'SRR12184956': ['SilNV',  'ERS9105435', 'Silurus sp. nidovirus'             ], ## submitted
    'SRR10402291': ['MalbNV', 'ERS9105436', 'Monopterus albus nidovirus'        ], ## submitted
    'SRR8389791' : ['HtraNV', 'ERS9105437', 'Hypomesus transpacificus nidovirus'] ## submitted
}

def load_seq(darth_side, org):
    ## How do I make this work for CoV genomes with more than one sequence?
    seq_records = []
    for record in SeqIO.parse(darth_side + '/transeq/canonical.fna', "fasta"):
        genome_ftr_counter = 0
        record.name = ''
        record.description = 'XXX'
        ref = Reference()
        ref.authors = 'XXX'
        ref.title = ''
        today = datetime.date.today().strftime('%d-%b-%Y').upper()
        ref.journal = 'Submitted ' + today + ' to the INSDC.'
        record.annotations['references'] = [ref]
        record.annotations['molecule_type'] = 'genomic RNA'
        record.annotations['topology'] = 'linear'
        record.annotations['accessions'] = ['XXX']
        record.annotations['sequence_version'] = 'XXX'
        record.annotations['data_file_division'] = 'XXX'
        sra_run_id = record.id.split('.')[0]
        record.id = 'SERRATUS-COV-' + sra_run_id.split('_')[0] + '_' + sra_run_id.split('_')[1]
        #org = 'Ambystoma mexicanum nidovirus'
        record.features.insert(0,
                               SeqFeature(FeatureLocation(0,
                                                          len(record)),
                                          type = 'source',
                                          strand = 1,
                                          qualifiers = {'mol_type': 'genomic RNA',
                                                        'organism': org }))
        seq_records.append(record)
    
    return seq_records

        ## Load VADR annots:

        ## Load 

def write_embl_file(genome_records, out_file):
    with open(out_file, 'wt') as embl_fh:
        SeqIO.write(genome_records, embl_fh, 'embl')

        
## Parse the VADR tab-delimited, five-column annotation format.
## See the following links for more info:
## * https://github.com/ncbi/vadr/blob/master/documentation/formats.md#annotate
## * https://www.ncbi.nlm.nih.gov/genbank/feature_table/

def load_vadr_annots(darth_side, run_id, genome_records):
    seen_lines = defaultdict(int)
    new_ftrs = []
    genome_ftr_counter = 0
    new_feature = None
    
    with open(darth_side + '/' + run_id + '/' + run_id + '.vadr.fail.tbl','r') as vadr_fh:
        for line in vadr_fh:
            ## VADR 'tbl' file has duplicate lines, use this to skip them:
            if line in seen_lines:
                continue
            else:
                seen_lines[line]
            
            ## Skip lines that are submitter notes:
            if not line.strip():
                break
            
            fields = line.rstrip().split('\t')
            
            ## Process a ">Feature" line:
            if line.startswith('>Feature'):
                id = fields[0].split()[1]
                segment_length = id.split('_')[3]
                coverage = id.split('_')[5]
                continue
            
            ### Process a feature line:
            if fields[0] and fields[1]:
                
                ## See if we need to add the new_feature:
                if len(fields) == 3 and new_feature is not None:
                    ## If VADR didn't specify the start codon, we make it explicit here for ENA submission:
                    if new_feature.type == 'CDS' and 'codon_start' not in new_feature.qualifiers:
                        new_feature.qualifiers['codon_start'] = '1'
                    assign_ftr2record(new_feature, genome_records)
                    new_feature = None
                
                ## Process the feature "start":
                if fields[0].startswith('<'):
                    start = BeforePosition(int(fields[0].strip('<')) - 1) ## Get it into BioPython crazy zero-indexed,
                                                                          ## half-interval convention:
                    ## StartsBefore
                else:
                    start = int(fields[0]) - 1
                    
                ## Process the feature "end":
                if fields[1].startswith('>'):
                    end = AfterPosition(int(fields[1].strip('>')))
                    ### Ends After
                else:
                    end = int(fields[1])
                
                ## If there is a third field, then we create a new feature
                ## else, we append the range to the existing feature:
                if len(fields) == 2:
                    new_region = FeatureLocation(start,end)
                    new_feature.location = new_feature.location + new_region
                else:                    
                    new_feature = SeqFeature(FeatureLocation(start,end),
                                             type = fields[2],
                                             strand = 1)

                continue
            
            ## Process a qualifier line:
            if fields[3] and fields[4]:
                if fields[3] == 'gene':
                    genome_ftr_counter += 1
                new_feature = process_qualifier_line(fields[3],
                                                     fields[4].replace("similar to ", ""),
                                                     genome_ftr_counter,
                                                     new_feature)
                
    ## See if we need to add the new_feature:
    if new_feature is not None:
        assign_ftr2record(new_feature, genome_records)
        
    ## Update the genome:
    #genome.features = new_ftrs
    
    return genome_records, coverage


def assign_ftr2record(new_feature, genome_records):

    len_segment1 = len(genome_records[0])

    ## The the feature end is less than the length of the first segment,
    ## so we allocate it to the first segment:
    if len(genome_records) == 1 or new_feature.location.end < len_segment1:
        genome_records[0].features.append(new_feature)

    ## The feature start is greater than the length of the first segment,
    ## so we allocate it to the second segment:
    elif new_feature.location.start >= len_segment1:
        new_feature.location = adjust_feature_location(new_feature.location,
                                                       len_segment1)
        genome_records[1].features.append(new_feature)

    ## The feature "straddles" the end of the first segment:
    ##
    ## If the feature is mostly on the first segment,
    ## then adjust the end coordinate, and allocate to the first segment:
    elif (len_segment1 - new_feature.location.start) > (new_feature.location.end - len_segment1):
        new_feature.location = adjust_feature_location(new_feature.location,
                                                       len_segment1,
                                                       clip_end_p = True)
        genome_records[0].features.append(new_feature)

    ## If the feature is mostly on the second segment,
    ## then adjust the start coordinate, and allocate to the second segment:
    elif (len_segment1 - int(new_feature.location.start)) < (int(new_feature.location.end) - len_segment1):
        new_feature.location = adjust_feature_location(new_feature.location,
                                                       len_segment1)
        genome_records[1].features.append(new_feature)
        
    ## If it is perfectly equal, throw an error!
    else:
        raise ValueError("I don't know how to allocate this feature!", new_feature)


## Scenarios that need to be covered:
## * Clip end of segment1 region (simple or compound)
## * Clip beginning of segment2 region (simple or compound) and adjust end position
## * Adjust all region positions (simple or compound)
## ^^^ All of these need to preserve the fuzzy positions!

def adjust_feature_location(feature_location, len_segment1, clip_end_p=False):

    ## Clip the end of a segment1 region that ends beyond the end of segment1:
    ## For a compond location, assume that just the last region needs to be clipped:
    if clip_end_p:
        new_loc = None
        loc_list = feature_location.parts
        for idx in range(len(loc_list)):
            curr_loc = loc_list[idx]
            if idx == 0 and len(loc_list) == 1:
                new_loc = FeatureLocation(curr_loc.start,
                                          AfterPosition(len_segment1-1))
            elif idx == 0:
                new_loc = FeatureLocation(curr_loc.start,
                                          curr_loc.end)
            elif idx == (len(loc_list)-1):
                new_loc = new_loc + FeatureLocation(curr_loc.start,
                                                    AfterPosition(len_segment1-1))
            else:
                new_loc = new_loc + curr_loc
        return new_loc

    ## Use Location arithmetic to translate the coordinates:
    translated_location = feature_location + (- len_segment1)

    ## 
    if translated_location.start < 0:
        new_loc = None
        loc_list = translated_location.parts
        for idx in range(len(loc_list)):
            curr_loc = loc_list[idx]
            if idx == 0:
                new_loc = FeatureLocation(BeforePosition(0),
                                          curr_loc.end)
            else:
                new_loc = new_loc + curr_loc
        translated_location = new_loc
        
    return translated_location


def process_qualifier_line(qual_key, qual_value, genome_ftr_counter, new_feature):
    
    if qual_key == 'gene':
        new_feature.qualifiers[qual_key] = qual_value
        new_feature.qualifiers['locus_tag'] = 'SERRATUS_gene_' + str(genome_ftr_counter)
        new_feature.qualifiers['inference'] = 'nucleotide motif:VADR:1.3'
    elif qual_key == 'product' and qual_value in nsp_desc:
        new_feature.qualifiers[qual_key] = qual_value
        new_feature.qualifiers['function'] = nsp_desc[qual_value]
        new_feature.qualifiers['inference'] = 'protein motif:VADR:1.3'
    elif qual_key == 'product' and new_feature.type == 'CDS':
        new_feature.qualifiers[qual_key] = qual_value
        new_feature.qualifiers['inference'] = 'protein motif:VADR:1.3'
    elif qual_key == 'product' and new_feature.type == 'mat_peptide':
        new_feature.qualifiers[qual_key] = qual_value
        new_feature.qualifiers['inference'] = 'protein motif:VADR:1.3'
    elif qual_key == 'note' and new_feature.type == 'stem_loop':
        new_feature.qualifiers[qual_key] = qual_value
        new_feature.qualifiers['inference'] = 'nucleotide motif:VADR:1.3'
    elif qual_key == 'protein_id':
        value_parts = qual_value.split('_')
        new_feature.qualifiers['protein_id'] = 'SERRATUS_prot_' \
            + value_parts[len(value_parts)-1]
    elif qual_key == 'exception' and qual_value == 'ribosomal slippage':
        ##new_feature.qualifiers['note'] = qual_value
        new_feature.qualifiers['ribosomal_slippage'] = None
    else:
        new_feature.qualifiers[qual_key] = qual_value

    return new_feature



def create_manifest_file(run_id,
                         coverage,
                         genome_records,
                         submission_dir,
                         project_id='PRJEB44047'):
    
    seq_platform = fetch_ncbi_seq_platform(run_id)
    with open(submission_dir + '/MANIFEST', 'w') as manifest_fh:
        print('\n'.join(['STUDY\t' + project_id,
                         'NAME\t' + sample_id_dict[run_id][0],
                         'SAMPLE\t' + sample_id_dict[run_id][1],
                         'RUN_REF\t' + run_id,
                         'MOLECULETYPE\tgenomic RNA',
                         'ASSEMBLY_TYPE\tisolate',
                         'COVERAGE\t' + coverage,
                         'PROGRAM\tcoronaSPAdes',
                         'PLATFORM\t' + seq_platform,
                         'FLATFILE\tgenome.embl.gz',
                         'CHROMOSOME_LIST\tchromosomes.gz']),
              file=manifest_fh)

def generate_genome_submission_files(darth_out_dir,
                                     run_id,
                                     project_id='PRJEB44047'):
    
    genome_records = load_seq(darth_out_dir,
                              sample_id_dict[run_id][2])

    ## Import VADR annots, for either one or two segments:
    new_genome_records, coverage = load_vadr_annots(darth_out_dir,
                                                    run_id,
                                                    genome_records)
    
    submission_dir = darth_out_dir + '/ena_submission'
    ## Create the output directory, if it doesn't already exist:
    os.makedirs(submission_dir,
                exist_ok=True)
    
    ## Print out the raw BioPython EMBL file:
    write_embl_file(new_genome_records,
                    submission_dir + '/genome.embl')
    
    ## Need to post-process the EMBL file:
    post_process_embl_file(submission_dir + '/genome.embl')
    
    ## Create the MANIFEST file:
    create_manifest_file(run_id,
                         coverage,
                         new_genome_records,
                         submission_dir)
    
    ## Create the chromosomes file:
    with gzip.open(submission_dir + '/chromosomes.gz', 'wt') as chrom_fh:
        chrom_num = 1
        for record in new_genome_records:
            print('\t'.join([record.id,
                             str(chrom_num),
                             'Linear-Chromosome']),
                  file=chrom_fh)
            chrom_num += 1

## Might need this for second segment of cannot get VADR to annotate it:
#def load_pfam_annots():
#    pass

## None of the Rfam models tested had E-values less than 0.01.
##def load_utr_annots():
##    pass




### Arguments:
##  * darth_side: path to darth


if __name__ == "__main__":
    args = sys.argv[1:]
    darth_side = args[0]
    run_id     = args[1]

    generate_genome_submission_files(darth_side,
                                     run_id)
