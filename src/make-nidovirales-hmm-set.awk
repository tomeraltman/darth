NR==FNR   { ids[$1]; next                }
/^HMMER3/ { print_p = 0; hmmer_line = $0 }
/^NAME/   { name_line=$0                 }
/^ACC/    { acc_line = $0
            gsub(/\..*$/,"",$2)
            #print $1, $2 > "/dev/stderr"
}
/^ACC/ && $2 in ids {
    #print $1, $2 > "/dev/stderr"
    print hmmer_line
    print name_line
    print acc_line
    print_p = 1
    next
}
print_p
