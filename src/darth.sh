#!/bin/bash -e

#### darth: Run VADR on Serratus assemblies.

accession="$1"
genome_path="$2"
reads_path="$3"
data_dir="$4"
output_parent_dir="$5"
num_cpus="$6"
debug="$7"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"

export PATH=$PATH:$DIR

## E-value threshold for hmmsearch:
E_value_cutoff="0.01"
Infernal_E_value_cutoff="1.0"

function run_vadr() {

    ## if the first run fails, try re-running without the protein alignment,
    ## to see if that dodges issue #205:

    ## --nomisc forces VADR to output high-quality annotation in the .tbl file(s),
    ## even if the hits are remote.
    v-annotate.pl \
	--mdir $data_dir/vadr-models-corona-1.3-3 \
	--mkey corona \
	--mxsize 64000 \
	-f \
	--keep \
	--nomisc \
	$output_parent_dir/transeq/concatenated.fna \
	$output_dir
    
    # || v-annotate.pl \
    # 	   --mdir $data_dir/vadr-models-corona-1.3-2 \
    # 	   --mkey corona \
    # 	   --mxsize 64000 \
    # 	   -f \
    # 	   --keep \
    # 	   --skip_pv \
    # 	   $output_parent_dir/transeq/canonical.fna \
    # 	   $output_dir	

    
    ## if VADR crashes, we re-run with the --skip_pv option:
    ### https://github.com/nawrockie/vadr/issues/21
    
}

output_dir="$output_parent_dir/$accession"


## Canonicalize the assembly contigs:
canonicalize_contigs.sh $genome_path $output_parent_dir $data_dir $E_value_cutoff

## If the Coronavirus genome has two segments,
## then concatenate the segments into a single segment
## which will be processed by VADR.
## The single defline contains the metadata for the second segment,
## so that the annotation can be separated out after-the-fact.

if [ "2" == `egrep -c "^>" $output_parent_dir/transeq/canonical.fna` ]
then
    
    awk 'NR == 1 { defline = $0; next }
         NR == FNR && /^>/ { gsub(/^>/,"");
                             split($0,name_parts,"_");
                             label2 = name_parts[1] "_" name_parts[2];
	                     length2 = name_parts[4];
		             coverage2 = name_parts[6];
			     next
         }
         NR == FNR { next }
         /^>/ && !printed_defline++ { 
              print $0, "label2=" label2, "length2=" length2, "coverage2=" coverage2; 
              next }
         !/^>/' \
	     $output_parent_dir/transeq/canonical.fna \
	     $output_parent_dir/transeq/canonical.fna \
	     > $output_parent_dir/transeq/concatenated.fna
else
    
    cp $output_parent_dir/transeq/canonical.fna $output_parent_dir/transeq/concatenated.fna
fi

if [ "$debug" == "canonicalize_only" ]
then
   exit
fi


## Try running VADR:
run_vadr


## Annotate ORF1ab with Pfam domains:
mkdir -p $output_parent_dir/pfam
pushd $output_parent_dir/pfam > /dev/null

## This for-loop is required because transeq silently deletes the long deflines coming from VADR (& before VADR, from SPAdes),
## and then all that is left is +_1, +_2, and +_3. Unfortunately I don't have time to debug this.
rm -f vadr-cds_3-frame-translated.fasta
for file in $output_dir/*.CDS.*.fa
do
    rm -f transeq_tmp.fsa
    defline=`head -n 1 $file`
    transeq -clean -frame F \
	    -sequence $file \
	    -outseq transeq_tmp.fsa
    awk -v defline="$defline" \
	'/^>/ { gsub(/^>\+/,""); print defline $0; next} 1' \
	transeq_tmp.fsa \
	>> vadr-cds_3-frame-translated.fasta
done

if [ `ls $output_dir | grep -c CDS.1.fa` == 1 ]
then
    # cat $output_dir/*.CDS.*.fa > vadr-cds.fna
    
    # transeq -clean -frame F \
    # 	    -sequence vadr-cds.fna \
    # 	    -outseq vadr-cds_3-frame-translated.fasta

    hmmsearch --max -E $E_value_cutoff \
	      -A match-alignments.sto \
	      --domtblout hmmsearch-matches.txt \
	      $data_dir/Pfam-A.SARS-CoV-2.hmm \
	      vadr-cds_3-frame-translated.fasta \
	      > hmmsearch-out.txt

    esl-sfetch --index vadr-cds_3-frame-translated.fasta

    grep -v "^#" hmmsearch-matches.txt \
	| awk '{ print $4"/"$20"-"$21, $20, $21, $1}' \
	| esl-sfetch -Cf vadr-cds_3-frame-translated.fasta - \
		     > vadr-cds_domains.fasta
    
    ## Pull out the alignments:
    gen-alignments-fasta.sh \
	hmmsearch-matches.txt \
	match-alignments.sto \
	> alignments.fasta
    
else
    echo "No CDS regions; not performing domain annotation via VADR gene calls." > /dev/stderr
fi

popd > /dev/null

## Convert VADR output to GFF

if [ -s $output_dir/$accession.vadr.pass.tbl ]
then
    tbl_file=$output_dir/$accession.vadr.pass.tbl
else
    tbl_file=$output_dir/$accession.vadr.fail.tbl
fi

tbl2gff.awk -v seqid="$accession" \
	    -v prog=vadr \
	    $tbl_file \
	    > $accession.vadr.gff


## Generate alternate gene calls:
mkdir -p $output_parent_dir/FragGeneScan
pushd $output_parent_dir/FragGeneScan > /dev/null

## Run FGS:
run_FragGeneScan.pl -genome=$genome_path \
		    -out=gene-calls \
		    -complete=1 \
		    -train=complete \
		    -thread=$num_cpus

## Annotate the proteins:
hmmsearch --max -E $E_value_cutoff \
	  -A match-alignments.sto \
	  --domtblout hmmsearch-matches.txt \
	  $data_dir/Pfam-A.SARS-CoV-2.hmm \
	  gene-calls.faa \
	  > hmmsearch-out.txt

popd > /dev/null

## Scan genome for UTRs:
mkdir -p $output_parent_dir/UTRs
pushd $output_parent_dir/UTRs > /dev/null
genome_length="`egrep -v "^>" $genome_path | tr -d '\n' | wc | awk '{print $3}'`"
cm_db_size="`awk -v genome_length=$genome_length 'BEGIN{print genome_length*2/1000000}'`"
cmscan \
    -Z $cm_db_size \
    -E $Infernal_E_value_cutoff \
    --nohmmonly \
    --tblout cmscan-utrs.tblout \
    --fmt 2 \
    --clanin $data_dir/coronavirus.clanin \
    $data_dir/coronavirus.cm \
    $genome_path \
    > cmscan-utrs_stdout.txt
popd > /dev/null

## Generate submission data for ENA:
python3 /usr/local/bin/all-you-need-is-cov.py "$output_parent_dir" "$accession"


## Self-align:

if [ "$reads_path" != "none" ]
then
    
    ### Read Analysis:
    mkdir -p $output_parent_dir/read-analysis
    pushd $output_parent_dir/read-analysis > /dev/null
    
    
    
    bowtie2-build $genome_path self_align
    bowtie2 --very-sensitive-local -x self_align -U $reads_path \
	| samtools view -S -b \
	| samtools sort \
		   > self-align-sorted.bam
    
    ## Generate variant data:
    bcftools mpileup -Ou \
	     -f $genome_path \
	     self-align-sorted.bam \
	| bcftools call -mv --ploidy 1 -Ob -o calls.bcf
    
    
    ## Generate convenience files for genome browsers like IGV and JBrowse:
    
    samtools index self-align-sorted.bam
    bcftools view calls.bcf > calls.vcf
    bgzip calls.vcf 
    tabix -p vcf calls.vcf.gz 
    samtools faidx $genome_path

    popd > /dev/null
fi


